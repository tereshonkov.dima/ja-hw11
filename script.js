"use strict"
// 1. Додати новий абзац по кліку на кнопку:
// По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

const btn = document.querySelector("#btn-click");
btn.addEventListener("click", () => {
    const newParagraph = document.createElement("p");
    const section = document.querySelector("#content");
    newParagraph.textContent = "New Paragraph";
    section.append(newParagraph);
});

// 2. Додати новий елемент форми із атрибутами:

// Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.

// По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.

const newBtn = document.createElement("button");
newBtn.textContent = "Input Create";
newBtn.id = "btn-input-create";
const section = document.querySelector("#content");
section.append(newBtn);

newBtn.addEventListener("click", () => {
    const input = document.createElement("input");
    input.type = "text";
    input.placeholder = "Enter your text";
    input.name = "name";
    
    const br = document.createElement("br");
    section.append(br);
    section.append(input);

});


